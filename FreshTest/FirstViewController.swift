//
//  FirstViewController.swift
//  FreshTest
//
//  Created by Fabio Martins on 9/22/18.
//  Copyright © 2018 Fabio Martins. All rights reserved.
//

import UIKit


class FirstViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func bringUpChat(_ sender: UIButton) {
        presentConversation(sender: self)
    }
    
    func presentConversation(sender: Any) {
        Freshchat.sharedInstance().showConversations(self)
    }
    
}

